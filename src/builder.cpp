//============================================================================
// Name        : builder
// Author      : René Haberland  (haberland1@mail.com)
// Date        : 14.08.2015, 2016, 2019, 2020
// Copyright   : - from 2015-2019   : CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/legalcode)
//             : - from 2020 ongoing: CC BY-NC 3.0 (https://creativecommons.org/licenses/by-nc-sa/3.0/legalcode)
//                  commercial use is forbidden without a prior written permission from the author
// Description : direct build script
//============================================================================

#ifndef BRANCH
 #define BRANCH "unknown branch"
#endif
#ifndef COMMIT
 #define COMMIT "unknown commit"
#endif

#include <algorithm>
#include <getopt.h>
#include <string>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <vector>
#include <sys/stat.h>

using namespace std;

#define BUILD_DIR 	"__BUILDER"
#define BUILD_CONFIG 	"builder.conf"
#define SHORT_STRING    256
#define LONG_STRING     5120
#define LLONG_STRING    524288

#ifdef THREADING
  #include <pthread.h>
  const unsigned NUM_THREADS = 4;       // same as CORES
  const unsigned MAX_BUILDER_CORES = 8; // maximum number of cpu cores supported (feel free to change)
  unsigned BUILDER_CORES=4;             // default (always !=0)
#endif

#ifdef MYMD5
    #include <stdio.h>
    #include <stdlib.h>
    #include <openssl/md5.h>
    #define BUILD_MD5SUMS   "MD5SUMS"
    #define MD5_HASH_CODE_LENGTH 33
    char fileMD5[MD5_HASH_CODE_LENGTH];
    vector<char*> _loaded_md5sums;      // MD5sums of files corresponding to _filelist
    vector<char*> _calculated_md5sums;  // MD5sums calculated by actually MD5-hashing files
    vector<int> _diff_list_md5sums;     // indices of source files that require compilation

    void calc_MD5sums();
    int load_MD5sums();
    void compare_MD5sums();
    char* calculate_md5(const char*);
    void update_md5sums_file();
#endif

bool BUILDER_SHOW_FILES=false;          // show file name while compiling or "." instead
bool BUILDER_SHOW_STATS =false;         // show benchmarking information per compiled file

time_t start_time, end_time;            // start and end time of main();  could be almost same job as 'time' from bash

FILE *f;    		                // some file descriptor
unsigned _curr_line_no=0;

char _proj[SHORT_STRING];
char _cc[SHORT_STRING], _flags[SHORT_STRING];
char _includes[LONG_STRING], _libs[LONG_STRING];
char proj_var[SHORT_STRING];
vector<char*> _filelist;                // all source files (after a MD5sums check untouched files may be dropped on compilation)
char cwd[LONG_STRING];
bool verbose=false;

void read_variable(const char* const, char* const);
void read_filelist();

void load_configuration();
void load_switches_env();
void* compile(void *);
void link();

void cleanup();

void usage() {
	printf("Usage: builder [OPTION]\n\n");
	printf(" René Haberland (haberland1@mail.ru), %s (%s,%s)", __DATE__, BRANCH, COMMIT); 
	printf("\n\tBuilds up a project with a configuration file.\n\n");
	
	printf("\t -h, --help, -? \t\t Displays this message.\n");
	printf("\t -c, --build-script[=CONFIGFILE] \t\t Use the CONFIGFILE specified.\n");
	printf("\t -v, --verbose \t\t Verbose mode. Debugging messages are printed.\n\n");

	printf("If no CONFIGFILE is specified 'builder.config' is searched by default.");
	printf(" Builder will attempt to create a '%s' directory where it will dump all intermediate and final files into.", BUILD_DIR);
	printf(" A configuration file has to have the following parts:\n\n");

	printf("\t CC=       \t specifies the compiler to be used\n");
	printf("\t INCLUDES= \t Include directories \n");
	printf("\t LIBS=     \t Libraries and pathes (e.g. -lopenssl or -LPATH)\n");
	printf("\t FLAGS=    \t Additional compilation flags (if non, keep empty)\n");
	printf("\t FILES=    \t Newline-separted source file list to be compiled\n\n");

	printf("Comments may be introduced to the configuration file with starting '#'.\n");
	printf("Compilation tweaks available via runtime shell variables:\n\n");

	printf("\t BUILDER_CORES      \t availabe cpu cores to be used\n");
	printf("\t BUILDER_SHOW_STATS \t displays runtime per PU (ticks) / total (s)\n");
	printf("\t BUILDER_SHOW_FILES \t display files processed during build\n");

	printf("\n");
}

int init_switches(int argc, char** argv){
	char* _p=getcwd(cwd,sizeof(cwd));
	if (_p == NULL){
		printf("ERROR: Could not refer to current working directory ! Use '-h' for help.\n\n");
		exit(12);
	}
	// use instead local 'builder.config' from cwd (is default), only to be considered without option -c
	strncpy(_proj, cwd, strlen(cwd));
	bool open_config_file = false;

	int help_flag = 0;
	const struct option longopts[] = {
		{ "help", no_argument, &help_flag, 1 },
		{ "build-script", optional_argument, NULL, 'c' },
		{ "verbose", no_argument, NULL, 'v' },
		{ 0 }
	};

	while (1) {
		int option = getopt_long(argc, argv, "hc::v", longopts, 0);  // opt returns -1 at EOF;  it also reads data into 'optarg'
		// 1 colon indicates argument is required
		// 2 colons indicates optional argument required by the (short) option
		if (option == -1) {
			break;
		}
		switch (option) {
			case 'h':
				help_flag = 1;
			break;
			case 'c':
			{
				if (optarg == NULL || !strncmp(optarg,"(null)",sizeof("(null)")) || !strncmp(optarg,"",sizeof(""))){
					f = fopen(BUILD_CONFIG,"r");
					if (f == NULL){
						printf("ERROR: Exiting because did not find '%s' in current directory ! Use '-h' for help.\n\n", BUILD_CONFIG);
						return 2;
					}
					open_config_file=true;
				}
				else{
					// check if there is such a provided configuration file
					f = fopen(optarg, "r");
					if (f == NULL){
						printf("ERROR: Could not find configuration file '%s' !\n\n", optarg);
						return 3;
					}
					open_config_file = true;
					// concate to project dir, result will be in '_proj'
					size_t i=0;
					while ((_proj[i]=cwd[i])) i++;
					size_t j = strlen(optarg);
					while (j>0 && optarg[j--] != '/') ;
					size_t n = j;
					if (n>0){
						j=0;
						_proj[i++]='/';
						while (j<=n && (_proj[i]=optarg[j])){
							++i;
							++j;
						}
					}
				}
			}
			break;
			case 'v':
				verbose = true;
			break;
			case '?':
				usage();
				return 1;
			default:
			break;
		}
	}
	if (help_flag){
		usage();
		return 1;
	}
	if (open_config_file == false){
		f = fopen(BUILD_CONFIG,"r");
		if (f == NULL){
			printf("ERROR: Exiting because did not find '%s' in current directory (when config file not specified) ! Use '-h' for help.\n\n", BUILD_CONFIG);
			return 2;
		}
	}

	return 0;
}

int main(int argc, char** argv) {
	int exit_code = init_switches(argc, argv);
	if (exit_code){
		exit(exit_code);
	}
	load_configuration(); // reads _filelist
	load_switches_env();
#ifdef MYMD5
	calc_MD5sums();       // calculate MD5-sums from all source files
	if (load_MD5sums()){  // load _md5sums from previous compilations
		// prepare diff-file-list for a full rebuild:
		_diff_list_md5sums.clear();
		for (int i=0;i<_filelist.size();i++){
			_diff_list_md5sums.push_back(i);
		}
	}
	compare_MD5sums();    // compare calculated with loaded sums
#endif

	if (BUILDER_SHOW_STATS){
		start_time = time(NULL);
	}

	// check and create dir __BUILDER only once (since dir is shared among threads):
	{
		char source_file[LONG_STRING];
		sprintf(source_file, "%s/%s", _proj, BUILD_DIR);

		if (access(source_file,F_OK)){
			if (mkdir((const char*)source_file,0777)){
			printf("ERROR: Could not create temporary directory '%s'!\n\n", BUILD_DIR);
			exit(11);
			}
		}
	}

#ifndef THREADING
	compile(NULL); // default routine
#else
	printf("In total %lu file(s) and %u core(s) available\n", _filelist.size(), BUILDER_CORES);
#ifdef MYMD5
	printf("%lu files to compile.\n", _diff_list_md5sums.size());
#endif

	pthread_t threads[NUM_THREADS];
	int tmp;
	int args[NUM_THREADS] = {0,1,2,3};
	pthread_attr_t attr;

	tmp = pthread_attr_init(&attr);
	if (tmp){
		printf("Init attributes of thread failed with error code %d!\n", tmp);
		exit(EXIT_FAILURE);		
	}
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_attr_setscope(&attr,PTHREAD_SCOPE_SYSTEM);

	// creating threads
	for(unsigned i=0;i<NUM_THREADS;i++){
		tmp=pthread_create(&threads[i],&attr,compile,(void*)&args[i]);
		if (tmp){
			printf("Creating thread %u failed with error %d!\n", i, tmp);
			exit(EXIT_FAILURE);
		}
	}

	// destroy thread attributes since not needed anymore
	tmp = pthread_attr_destroy(&attr);
	if (tmp){
		printf("Destroying attributes of thread failed with error code %d!\n", tmp);
		exit(EXIT_FAILURE);
	}
	

	// joining threads
	for(unsigned i=0;i<NUM_THREADS;i++){
		tmp=pthread_join(threads[i],NULL);    // not interested in result, hence NULL
		if (tmp){
			printf("Joining thread %u failed !\n", i);
			exit(EXIT_FAILURE);
		}
	}
#endif

	link();
#ifdef MYMD5
	update_md5sums_file();
#endif

	cleanup();

	if (BUILDER_SHOW_STATS){
		end_time = time(NULL);
		printf("Time elapsed in total %ld second(s)\n", end_time-start_time);
	}
	return 0;
}


void cleanup(){
  for (auto it=_filelist.begin();it!=_filelist.end();++it){
    free(*it);
  }
  _filelist.clear();

  for (auto it=_calculated_md5sums.begin();it!=_calculated_md5sums.end();++it){
    free(*it);
  }

  for (auto it=_loaded_md5sums.begin();it!=_loaded_md5sums.end();++it){
    free(*it);
  }

  _calculated_md5sums.clear();
  _loaded_md5sums.clear();
}

#ifdef MYMD5
void update_md5sums_file(){
	char md5_file[LONG_STRING];
	sprintf(md5_file,"%s/%s/%s",_proj,BUILD_DIR,BUILD_MD5SUMS);

	f = fopen(md5_file,"w");
	if (f == NULL){
		printf(" ERROR: could not write to MD5sum-file %s\n", md5_file);
		return;
	}
	for (auto it=_calculated_md5sums.begin();it!=_calculated_md5sums.end();++it){
		fprintf(f,"%s\n", *it);
	}
	fclose(f);
	printf("Written %lu md5sums to %s\n", _calculated_md5sums.size(), BUILD_MD5SUMS);
}
#endif

/*
 * Attempts to scan current text line with open file descriptor
 *  Halts on errors with error message.
 *
 *  fan-out: var_value (to be freed by the calling instance)
 */
void read_variable(const char* const var_name, char* const var_value){
	char* _line = NULL;
	size_t _len = 0;
	ssize_t nread;
	++_curr_line_no;
	do{
		nread = getline(&_line, &_len, f);
		if (_line == NULL){
			printf("ERROR: Could not find variable '%s' down to line %d\n\n", var_name, _curr_line_no);
			free(_line);
			exit(77);
		}
	}while (_line[0] == '#');
	if (nread <= 0 || var_name == NULL || _line == NULL){
		printf("ERROR: Corrupt or invalid configuration file at line %d\n\n", _curr_line_no);
		exit(7);
	}
	int j=0;
	while (var_name[j] == _line[j]){
		j++;
	}
	if (_line[j++] != '=' || j == 0){
		printf("ERROR: EQUAL expected at line %d\n\n", _curr_line_no);
		exit(8);
	}
	int size = nread-j-1;
	for (int dj=0;dj<size;dj++){
		var_value[dj] = _line[j++];
	}
	free(_line);
}

/*
 * fan-out: _filelist
 */
void read_filelist(){
	char* _line = NULL;
	size_t _len = 0;
	ssize_t nread;
	do{
		nread = getline(&_line, &_len,f);
	}while(nread>0 && _line!=NULL && _line[0] == '#');
	if (nread <= 0 || _line == NULL || strcmp(_line, "FILES=\n")){
		printf("ERROR: Missing 'FILES=' at line %d\n\n", _curr_line_no);
		free(_line);
		exit(7);
	}
	long l_pos = ftell(f);
	unsigned files_count=0;
	while ((nread=getline(&_line,&_len,f))!=-1){
		if (nread>0 && _line[0] != '#'){
			++files_count;
		}
	}
	fseek(f,l_pos,SEEK_SET);
	_filelist.resize(files_count);
	for (unsigned j=0, jj=0;jj<files_count;j++){
		do{
			nread=getline(&_line,&_len,f);
		}while(nread>0 && _line==NULL && _line[0] == '#');
		char* _line2=(char*)malloc(nread+1);
		strncpy(_line2,_line,nread+1);
		_line2[nread-1]='\0';
		_filelist[jj++] = _line2;
	}
	fclose(f);
	free(_line);
}

void load_configuration(){
	read_variable("CC", _cc);
	read_variable("INCLUDES", _includes);
	read_variable("LIBS", _libs);
	read_variable("FLAGS", _flags);
	read_filelist();
}

void load_switches_env(){
#ifdef THREADING
	const char* s;
	// 1) read CPU cores:
	s = getenv("BUILDER_CORES"); // make visible to builder, e.g. export BUILDER_CORES=4
	if (s!=NULL){
		int i=atoi(s);
		if (i==0 || i>MAX_BUILDER_CORES){
			printf(" Warning: BUILDER_CORES=%d ignored, resetting to default: %d\n",i,BUILDER_CORES);
		}
		else{
			BUILDER_CORES=i;
			printf(" BUILDER_CORES set to %d\n", BUILDER_CORES);
		}
	}
	// 2) BUILDER_SHOW_FILES, e.g. export BUILDER_SHOW_FILES=y
	s = getenv("BUILDER_SHOW_FILES");
	if (s!=NULL){
		printf(" BUILDER_SHOW_FILES set\n");
		BUILDER_SHOW_FILES=true;
	}
	// 3) BUILDER_SHOW_STATS, e.g. export BUILDER_SHOW_STATS=y
	s = getenv("BUILDER_SHOW_STATS");
	if (s!=NULL){
		printf(" BUILDER_SHOW_STATS set\n");
		BUILDER_SHOW_STATS=true;
	}
#endif
}

#ifdef MYMD5
void calc_MD5sums(){
	char full_src_file[LONG_STRING];

	_calculated_md5sums.clear();
	for (auto it=_filelist.begin(); it!=_filelist.end();++it){
		sprintf(full_src_file, "%s/%s", _proj, *it);
		char* md5_hash = calculate_md5(full_src_file);
		if (md5_hash == NULL || md5_hash[0]=='\0'){
			printf(" ERROR: in calc_MD5sums an invalid MD5hash was found for source file: %s\n. Check this suspicious file !", full_src_file);
			exit(4);
		}

		char* md5_hash_cpy = (char*)malloc(MD5_HASH_CODE_LENGTH);
		strncpy(md5_hash_cpy,md5_hash,MD5_HASH_CODE_LENGTH);
		md5_hash_cpy[MD5_HASH_CODE_LENGTH-1]='\0'; // trim rest, if any, for safety

		_calculated_md5sums.push_back(md5_hash_cpy);
	}
}

/*
 * 0 = success,
 * otherwise signal full rebuild
 */
int load_MD5sums(){
	_loaded_md5sums.clear();
	char* _p=getcwd(cwd,sizeof(cwd));
	if (_p == NULL){
		printf("ERROR: Could not cwd in load_MD5sums !\n\n");
		exit(12);
	}
	char md5_file[LONG_STRING];
	sprintf(md5_file, "%s/%s/%s", _proj, BUILD_DIR, BUILD_MD5SUMS);

	f = fopen(md5_file,"r");   // opens and positions to beginning
	if (f == NULL){
		printf(" DEBUG: full rebuild required (e.g. no MD5sums)\n");
		return 1;
	}

	char* _line = NULL;
	size_t _len = 0;
	ssize_t _nread;
	while ((_nread = getline(&_line,&_len,f)) != -1){
		// allocating _nread Bytes may not always be safe!
		char* _line_cpy = (char*)malloc(MD5_HASH_CODE_LENGTH);
		strncpy(_line_cpy,_line,MD5_HASH_CODE_LENGTH);
		// do not read newline OR further bytes in case of longer hashes
		_line_cpy[MD5_HASH_CODE_LENGTH-1]='\0';
		// do not free _line here, since its content is needed later!
		_loaded_md5sums.push_back(_line_cpy);
		// do not free _line_cpy here, otherwise _loaded_md5sums is freed, which will force recompilation of all!
	}
	free(_line);

	if (_loaded_md5sums.size() != _filelist.size()){
		printf(" DEBUG: md5sum(s) indicate(s) source repository has changed since last build\n");
		return 1;
	}
	fclose(f);

	return 0;  // indicate that no full rebuild is required
}

char* calculate_md5(const char* fname)
{
	const unsigned BUFLEN = 1024;
	unsigned char md[MD5_DIGEST_LENGTH];
	MD5_CTX md5Ctx;
	unsigned char buf[BUFLEN];
	int bytes_read;
	
	memset(fileMD5, '\0', MD5_HASH_CODE_LENGTH);

	FILE* fin = fopen(fname, "rb");
	if (!fin) {
		perror(fname);
		return NULL;
	}

	MD5_Init(&md5Ctx);
	while (bytes_read = fread(buf, 1, BUFLEN, fin))
		MD5_Update(&md5Ctx, buf, bytes_read);
	MD5_Final(md,&md5Ctx);

	for (int i=0; i<MD5_DIGEST_LENGTH; i++)
		snprintf(&fileMD5[i*2], MD5_HASH_CODE_LENGTH, "%02x", (unsigned)md[i]);

	fclose(fin);
	return fileMD5;
}

/*
 * may trim _filelist (files not needed for compilation are going to be dropped,
 *  but all files are still required for linking)
 */
void compare_MD5sums(){
	if (_calculated_md5sums.size() != _filelist.size()){
		return; // do not skip any file in _filelist, since it all will be build
	}
	if (_calculated_md5sums.size() != _loaded_md5sums.size()){
		return; // ditto
	}

	int sz = _calculated_md5sums.size();
	_diff_list_md5sums.clear();
	for (int i=0;i<sz;i++){
		if (strncmp(_calculated_md5sums[i], _loaded_md5sums[i], MD5_HASH_CODE_LENGTH)){
			// if MD5sums do not match, then (re-)compilation is required
			_diff_list_md5sums.push_back(i);
		}
		else{
			// expected and actual MD5 hashes match
			// still check whether the correspondent .o-file exists
			char object_file[LONG_STRING];
			sprintf(object_file, "%s/%s/%d.o", _proj, BUILD_DIR,i);
			if (access(object_file,F_OK)){
				// no access, so add to diff-list
				_diff_list_md5sums.push_back(i);
			}
		}
	}
}
#endif

/*
 * no return value; abrupt termination on error
 */
void* compile(void *arg){
	char compile_cmd[LONG_STRING];

#ifdef THREADING
	int FILES = _filelist.size();

	int thread_id;
	thread_id = *((int*)arg);
	int is_last = 0;
	int old_width=-1;

	int width=FILES/BUILDER_CORES;
	if (FILES%BUILDER_CORES){
		++width;
		// last thread contains the rest
		if (thread_id == BUILDER_CORES-1){
			is_last = 1;
			old_width = width;
			width=FILES-(old_width*(BUILDER_CORES-1));
		}
	}
	int start_idx = (is_last)?thread_id*old_width:thread_id*width;   // the file-list slice for compilation
	int stop_idx=start_idx+width-1;

	if (start_idx>FILES-1){ // in cases where there are more cpu cores than files available skip remaining cores
		start_idx=-1;
		stop_idx=-2;
	}
	// there are 'width' files to be processed in this thread #'thread_id'
	// starting with file 'start_idx' and ending at 'stop_idx' (in FILES)
#else
	int start_idx=0;
	int stop_idx=_filelist.size()-1;
#endif

	for(unsigned i=start_idx;i<=stop_idx;i++){

#ifdef MYMD5
		if (std::find(_diff_list_md5sums.begin(), _diff_list_md5sums.end(), i) == _diff_list_md5sums.end()){
			// not in diff-file-list, so skip compilation for that file
			continue;
		}
#endif

		sprintf(compile_cmd,"%s -c %s %s %s/%s -o %s/%s/%d.o",_cc,_flags,_includes,_proj,_filelist[i],_proj,BUILD_DIR,i);

		clock_t finish; // initialise every time with actual time
		int ret = system(compile_cmd);
		if (WEXITSTATUS(ret)){
			printf("ERROR: Cannot compile '%s'!\n\n", _filelist[i]);
			// even and especially in multi-threading mode quit application immediately (quit on 1st error strategy)
			exit(13);
		}
		finish = clock();

		if (verbose == true){
#ifndef THREADING
			printf("COMPILED: %s\n", compile_cmd);
#else
			printf("COMPILED(#%d): %s\n", thread_id, compile_cmd);
#endif
		}
		else{
			if (BUILDER_SHOW_FILES){
				printf("%s",_filelist[i]);
				if (!BUILDER_SHOW_STATS){
					printf("\n"); fflush(0);
				}
			}
			else{
				printf("."); fflush(0);
			}
		}

		if (BUILDER_SHOW_STATS){
			printf (" %ld ticks\n", (long)finish); fflush(0);
		}
	}
	printf("\n");
}

void link(){
	char link_cmd[LLONG_STRING];
	sprintf(link_cmd, "%s -o %s/%s/main", _cc, _proj, BUILD_DIR);

	for (unsigned i=0;i<_filelist.size();i++){
		sprintf(link_cmd, "%s %s/%s/%d.o", link_cmd, _proj, BUILD_DIR, i);
	}
	sprintf(link_cmd, "%s %s", link_cmd, _libs);

#ifdef MYMD5
	if (_diff_list_md5sums.size()>0){
#endif
		if (verbose == true){
			printf("LINK: %s\n", link_cmd);
		}
		int ret = system(link_cmd);
		if (WEXITSTATUS(ret)){
			printf("ERROR: Linking error with command:\n %s\n\n", link_cmd);
			exit(13);
		}
#ifdef MYMD5
	}
	else{
		if (verbose == true){
			printf("LINKING skipped, since no file changed! \n");
			printf(" LINK command not executed: %s\n", link_cmd);
		}
	}
#endif
}
