NAME:

Builds up a software project with a configuration file.


SYNOPSIS:

builder [OPTION]

 -h, --help, -? 		 Displays this message.
 -c, --build-script[=CONFIGFILE] 		 Use the CONFIGFILE specified.
 -v, --verbose 		 Verbose mode. Debugging messages are printed.


DESCRIPTION:

builder builds an arbitrary software project in two stages: compiling/static analysis and (optional) linking. builder is similar to GNU Make, except it's only purpose is for building software from sources. Necessary environment variables may be imported to the build configuration script. Non-building operations (like file manipulations beside compilation/linking) shall be extracted from the build process and shall therefore vanish. All files are compiled straight forward as provided by FILES from the config file, and shall be dumped in a temporary directory __BUILDER, which will contain the final binary file in case linking is successful.
    

If no CONFIGFILE is specified 'builder.config' is searched by default.
 Builder will attempt to create a '%s' directory where it will dump all intermediate and final files into.
 A configuration file has to have the following parts:

 CC=          specifies the compiler to be used
 INCLUDES=    Include directories
 LIBS=        Libraries and pathes (e.g. -lopenssl or -LPATH)
 FLAGS=       Additional compilation flags (if non, keep empty)
 FILES=       Newline-separted source file list to be compiled

Comments may be introduced to the configuration file with starting '#'.

Compilation tweaks available via runtime shell variables:

 BUILDER_CORES       availabe cpu cores to be used
 BUILDER_SHOW_STATS  displays runtime per PU (ticks) / total (s)
 BUILDER_SHOW_FILES  display files processed during build

Since the 2020 version the MD5 module from the default 'crpyto'-library is used, which is considered a 3rd-party contribution.
 The MD5-module is under Mozilla-licence and should not infringe the license terms of this tool. As a precaution the library may be dropped from usage by simply not using it as specified in the Makefile as commented out.

