BRANCH:=$(shell git branch | awk '{ print $$2 }')
COMMIT:=$(shell git log -n1 | grep "^commit" | awk '{ print $$2 }' | cut -c1-7)

all:
	mkdir -p bin
	g++ -O0 -g -o bin/builder src/builder.cpp -lcrypto -lpthread -DMYMD5 -DTHREADING -DBRANCH=\"$(BRANCH)\" -DCOMMIT=\"$(COMMIT)\"

build-nothreading:
	mkdir -p bin
	g++ -O0 -g -o bin/builder src/builder.cpp  -DBRANCH=\"$(BRANCH)\" -DCOMMIT=\"$(COMMIT)\"

build-nomd5:
	mkdir -p bin
	g++ -O0 -g -o bin/builder src/builder.cpp -lpthread -DTHREADING -DBRANCH=\"$(BRANCH)\" -DCOMMIT=\"$(COMMIT)\"

build-nomd5nothreading:
	mkdir -p bin
	g++ -O0 -g -o bin/builder src/builder.cpp -DBRANCH=\"$(BRANCH)\" -DCOMMIT=\"$(COMMIT)\" 


clean:
	rm -fR bin/ examples/sample1/__BUILDER examples/gears/__BUILDER examples/cloth/__BUILDER examples/htop/__BUILDER

test:
	$(MAKE) -C ./examples/sample1 -f Makefile all > /dev/null 2>&1
	$(MAKE) -C ./examples/gears -f Makefile all
	$(MAKE) -C ./examples/cloth -f Makefile all
	$(MAKE) -C ./examples/htop -f Makefile all
