#ifndef CLOTHSPRING_CPP_
#define CLOTHSPRING_CPP_

#include "clothspring.hpp"
#include "clothparticle.hpp"

ClothSpring::ClothSpring() {
  create(0, 0);
}

void ClothSpring::create(ClothParticle* particle_A, ClothParticle* particle_B) {
  stiffness = 1.0;
  this->particle_A = particle_A;
  this->particle_B = particle_B;
}

#endif
