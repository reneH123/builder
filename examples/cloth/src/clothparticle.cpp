#ifndef CLOTHPARTICLE_CPP_
#define CLOTHPARTICLE_CPP_

#include "clothparticle.hpp"

ClothParticle::ClothParticle() {
	links = new ClothSpring*[12];
	position = velocity = acceleration = force = Vector3D::ZERO();
	fixed = false;
	mass = 1.0;
	damping = 0.0;
}

#endif
