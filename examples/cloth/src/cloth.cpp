#ifndef CLOTH_CPP_
#define CLOTH_CPP_

#include "cloth.hpp"
#include "include.hpp"

#include <GL/gl.h>
#include <GL/glu.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "bitmap.h"

Cloth::Cloth(void) {
	Initialize();
}

Cloth::~Cloth(void) {
	if (m_Particles)
		delete[] m_Particles;
	if (m_Springs)
		delete[] m_Springs;
	if (m_old_Positions)
		delete[] m_old_Positions;
	if (faces)
		delete[] faces;
}

void Cloth::LoadGLTexture() {
	unsigned char* TextureImage = NULL;

	CBitmap *bm = new CBitmap("res/desktop.bmp");
	TextureImage = (unsigned char*)bm->m_BitmapData;

	if (TextureImage){
		glGenTextures(1, &texture[0]);
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bm->m_BitmapHeader.Width,
				bm->m_BitmapHeader.Height, 0,
				GL_RGB,
				GL_UNSIGNED_BYTE, TextureImage);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	if (TextureImage) {
			free(TextureImage);
	}
}

void Cloth::Initialize() {
	int i;

	// setting up default values
	this->normals_Activated = 0;
	this->Euler_Activated = 1;
	this->RungeKutta_Activated = 0;
	this->MidPoint_Activated = 0;

	this->m_ResolutionX = this->m_ResolutionY = 15; // 20 is known to be stable
	this->m_ClothMass = 1;
	this->m_StructuralStiffness = 3;
	this->m_ShearStiffness = 5;
	this->m_FlexionStiffness = 1;
	this->m_Damping = 0.01;
	this->m_Fluid = 0.014;
	this->m_Friction = 100;
	this->m_ClothStartPosition = Vector3D(-10, 5, -12);
	// create particle grid
	this->m_Particles = new ClothParticle*[m_ResolutionX * m_ResolutionY];
	// initialize particle grid
	for (i = 0; i < m_ResolutionX * m_ResolutionY; i++) {
		m_Particles[i] = new ClothParticle();
	}
	// create spring list
	m_Springs = new ClothSpring*[m_ResolutionX * m_ResolutionY * 12];
	// initialize particle grid
	for (i = 0; i < m_ResolutionX * m_ResolutionY * 12; i++) {
		m_Springs[i] = 0;
	}
	// generate cloth structure
	InitializeClothStructure();
	// calculate initial spring length
	i = 0;
	while (m_Springs[i]) {
		m_Springs[i]->initialLength = (m_Springs[i]->particle_A->position
				- m_Springs[i]->particle_B->position).length();
		i++;
	}
	//
	// create face list with 4 directions (nw=0, ne=1, sw=2, se=3)
	faces = (Facette*) malloc(
			sizeof(Facette) * (m_ResolutionX - 1) * (m_ResolutionY - 1));

	InitializeFaces();
	m_old_Positions = new Vector3D*[m_ResolutionX * m_ResolutionY];
	collisionProxy = NULL;
	ground = new Ground(); //
	// use gray color if could not load texture
	LoadGLTexture();
	glEnable(GL_TEXTURE_2D);
	// z-buffers
	glEnable(GL_DEPTH_TEST);
	// spotlight and material settings
	GLfloat ambi[4] = { 0.8, 0.3, 0.7, 1 };
	GLfloat spek[4] = { 1.0, 1.0, 1.0, 1 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambi);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spek);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.9);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	GLfloat position0[4] = { 0, 2, -2 };
	GLfloat position1[4] = { 2, 4, -2, 1 };
	GLfloat color0[4] = { 0.1, 0.3, 0.2 };
	GLfloat color1[4] = { 0, 0, 1, 0.7 };
	glLightfv(GL_LIGHT0, GL_POSITION, position0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, color0);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.3);
	glLightfv(GL_LIGHT1, GL_POSITION, position1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, color1);
}

void Cloth::ReInitialize() {
	InitializeClothStructure();
}

void Cloth::InvertNormalsActivated() {
	inv(normals_Activated);
}

void Cloth::ChooseIntegrator(int solver) {
	switch (solver) {
	case 0: {
		Euler_Activated = 1;
		RungeKutta_Activated = 0;
		MidPoint_Activated = 0;
		break;
	}
	case 1: {
		Euler_Activated = 0;
		RungeKutta_Activated = 1;
		MidPoint_Activated = 0;
		break;
	}
	case 2: {
		Euler_Activated = 0;
		RungeKutta_Activated = 0;
		MidPoint_Activated = 1;
		break;
	}
	}
}

void Cloth::Paint() {
	if (collisionProxy)
		collisionProxy->Paint();
	//if (ground)   // TODO
		//ground->Paint();
	// draw only if there are particles
	if (!m_Particles)
		return;
	// re-adjust initial orientation
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	// choose proper texture
	//glBindTexture(GL_TEXTURE_2D, texture[0]);  // TODO!
	int maxFaces = (m_ResolutionX - 1) * (m_ResolutionY - 1);
	for (int i = 0; i < maxFaces; i++) {
		glColor3f(1, 1, 1);
		if (normals_Activated) {
			// draw normals
			glBegin(GL_LINES);
			glNormal3dv(m_Particles[faces[i].nw]->normal.array);
			glVertex3dv(m_Particles[faces[i].nw]->position.array);
			glVertex3dv(
					(m_Particles[faces[i].nw]->position
							+ m_Particles[faces[i].nw]->normal).array);
			glEnd();
		}
		// draw textured or grey rectangle
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3dv(m_Particles[faces[i].no]->position.array);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3dv(m_Particles[faces[i].nw]->position.array);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3dv(m_Particles[faces[i].sw]->position.array);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3dv(m_Particles[faces[i].so]->position.array);
		glEnd();
	}
}

void Cloth::SolveImplicitEuler(double delta_t) {
	static ClothParticle *particle;
	static int maxParticles = m_ResolutionX * m_ResolutionY;

	for (int i = 0; i < maxParticles; i++) {
		particle = m_Particles[i];
		if (particle->fixed == false) {
			particle->velocity += particle->acceleration * delta_t;
			particle->position += particle->velocity * delta_t;
			m_Particles[i] = particle;
		}
	}
}

void Cloth::MidPointMethod(double delta_t) {
	static ClothParticle *particle;
	static int maxParticles = m_ResolutionX * m_ResolutionY;

	for (int i = 0; i < maxParticles; i++) {
		particle = m_Particles[i];
		if (particle->fixed == false) {
			// v(2dt):= v + a*dt
			particle->velocity += particle->acceleration * delta_t * 0.5;
			particle->position += particle->velocity * delta_t;
			m_Particles[i] = particle;
		}
	}
}

void Cloth::SolveRungeKutta(double delta_t) {
	static ClothParticle *particle;
	Vector3D k1, k2, k3, k4;
	static int maxParticles = m_ResolutionX * m_ResolutionY;

	for (int i = 0; i < maxParticles; i++) {
		particle = m_Particles[i];
		if (particle->fixed == false) {
			// v':= v + a*dt = f(t,x(t))
			k1 = particle->velocity; // v'(t0=0)
			k2 = k1 + particle->acceleration * delta_t * 0.5;
			k3 = k2 + particle->acceleration * delta_t * 0.5;
			k4 = k3 + particle->acceleration * delta_t;

			particle->velocity = (k1 + (k2 * 2) + (k3 * 2) + k4)
					* ((double) 1 / 6);
			particle->position += particle->velocity * delta_t;

			m_Particles[i] = particle;
		}
	}
}

void Cloth::Step(double delta_t) {
	int x, y;

	CalculateInternalForce(delta_t);
	CalculateExternalForces(delta_t);
	testCollision(collisionProxy);
	testCollision(ground);

	// save old particles
	for (y = 0; y < m_ResolutionY; y++) {
		for (x = 0; x < m_ResolutionX; x++) {
			m_old_Positions[x + y * m_ResolutionX] = &m_Particles[x
					+ y * m_ResolutionX]->position;
			m_Particles[x + y * m_ResolutionX]->normal = getNormal(x, y);
		}
	}
	// solver for calculating new particle positions - e.g. Forward Euler Integration
	if (Euler_Activated)
		SolveImplicitEuler(delta_t);
	else {
		if (RungeKutta_Activated)
			SolveRungeKutta(delta_t);
		else
			MidPointMethod(delta_t);
	}

	// recalculate all particle normals
	for (y = 0; y < m_ResolutionY; y++)
	 for (x = 0; x < m_ResolutionX; x++)
	 m_Particles[x + y * m_ResolutionX]->normal = getNormal(x, y);
}

void Cloth::InitializeClothStructure() {
	ClothParticle *cp;
	ClothSpring *cs;
	// stepping through every particle
	for (int y = 0; y < m_ResolutionY; y++)
		for (int x = 0; x < m_ResolutionX; x++) {
			cp = m_Particles[x + y * m_ResolutionX];
			// setting up particle attributes
			cp->mass = m_ClothMass / ((double) m_ResolutionX * m_ResolutionY);
			cp->position = Vector3D((double) x, 0, (double) y)
					+ m_ClothStartPosition;
			cp->damping = m_Damping;
			cp->velocity = Vector3D::ZERO();
			cp->acceleration = Vector3D::ZERO();
			cp->force = Vector3D::ZERO();
			cp->normal = Vector3D::ZERO();

			cp->fixed = false;
			// Building up Particle Linkings
			for (int i = 0; i < 12; i++) {
				cs = cp->links[i] = 0;
				switch (i) {
				case 0: // StructureSpring North
					if (y > 0)
						cs = findSpring(cp,
								m_Particles[x + (y - 1) * m_ResolutionX],
								ClothSpring::Structural);
					break;
				case 1: // StructureSpring South
					if (y < m_ResolutionY - 1)
						cs = findSpring(cp,
								m_Particles[x + (y + 1) * m_ResolutionX],
								ClothSpring::Structural);
					break;
				case 2: // StructureSpring East
					if (x > 0)
						cs = findSpring(cp,
								m_Particles[x - 1 + y * m_ResolutionX],
								ClothSpring::Structural);
					break;
				case 3: // StructureSpring West
					if (x < m_ResolutionX - 1)
						cs = findSpring(cp,
								m_Particles[x + 1 + y * m_ResolutionX],
								ClothSpring::Structural);
					break;

				case 4: // ShearSpring NorthWest
					if ((y > 0) && (x < m_ResolutionX - 1))
						cs = findSpring(cp,
								m_Particles[x + 1 + (y - 1) * m_ResolutionX],
								ClothSpring::Shear);
					break;
				case 5: // ShearSpring NorthEast
					if ((y > 0) && (x > 0))
						cs = findSpring(cp,
								m_Particles[x - 1 + (y - 1) * m_ResolutionX],
								ClothSpring::Shear);
					break;
				case 6: // ShearSpring SouthWest
					if ((y < m_ResolutionY - 1) && (x < m_ResolutionX - 1))
						cs = findSpring(cp,
								m_Particles[x + 1 + (y + 1) * m_ResolutionX],
								ClothSpring::Shear);
					break;
				case 7: // ShearSpring SouthEast
					if ((y < m_ResolutionY - 1) && (x > 0))
						cs = findSpring(cp,
								m_Particles[x - 1 + (y + 1) * m_ResolutionX],
								ClothSpring::Shear);
					break;

				case 8: // FlexionSpring North
					if (y > 1)
						cs = findSpring(cp,
								m_Particles[x + (y - 2) * m_ResolutionX],
								ClothSpring::Flexion);
					break;
				case 9: // FlexionSpring South
					if (y < m_ResolutionY - 2)
						cs = findSpring(cp,
								m_Particles[x + (y + 2) * m_ResolutionX],
								ClothSpring::Flexion);
					break;
				case 10: // FlexionSpring East
					if (x > 1)
						cs = findSpring(cp,
								m_Particles[x - 2 + y * m_ResolutionX],
								ClothSpring::Flexion);
					break;
				case 11: // FlexionSpring West
					if (x < m_ResolutionX - 2)
						cs = findSpring(cp,
								m_Particles[x + 2 + y * m_ResolutionX],
								ClothSpring::Flexion);
					break;
				}
				if (cs) {
					if (i < 4)
						cs->stiffness = m_StructuralStiffness;
					else if (i < 8)
						cs->stiffness = m_ShearStiffness;
					else if (i < 12)
						cs->stiffness = m_FlexionStiffness;
				}
				cp->links[i] = cs;
			}
		}
}

Vector3D Cloth::det_f(int i, int j, int k, int l,
		ClothSpring::SpringTypes type) {
	ClothSpring *cspring = findSpring(m_Particles[j + i * m_ResolutionX],
			m_Particles[l + k * m_ResolutionX], type);
	Vector3D l_vec = cspring->particle_A->position
			- cspring->particle_B->position;
	Vector3D a = l_vec.Normalized() * (cspring->initialLength), // = l_{i,j,k,l}^{0}
	b = l_vec - a;
	return b * cspring->stiffness; // cspring->stiffness= K_{i,j,k,l}
}

void Cloth::CalculateInternalForce(double delta_t) {
	static Vector3D f;

	for (int i = 0; i < m_ResolutionY; i++) {
		for (int j = 0; j < m_ResolutionX; j++) {
			f = Vector3D::ZERO();
			// calculate all neighbours for p(i,j):
			if (i < m_ResolutionY - 1) {
				f = f + det_f(i, j, i + 1, j, ClothSpring::Structural);

				if (j < m_ResolutionX - 1)
					f = f + det_f(i, j, i + 1, j + 1, ClothSpring::Shear);
				if (i < m_ResolutionY - 2)
					f = f + det_f(i, j, i + 2, j, ClothSpring::Flexion);
			}
			if (i > 0) {
				f = f - det_f(i, j, i - 1, j, ClothSpring::Structural);

				if (j > 0)
					f = f - det_f(i, j, i - 1, j - 1, ClothSpring::Shear);
				if (i > 1)
					f = f - det_f(i, j, i - 2, j, ClothSpring::Flexion);
			}
			if (j < m_ResolutionX - 1) {
				f = f + det_f(i, j, i, j + 1, ClothSpring::Structural);

				if (i < m_ResolutionY - 1)
					f = f + det_f(i, j, i + 1, j + 1, ClothSpring::Shear);
				if (j < m_ResolutionX - 2)
					f = f + det_f(i, j, i, j + 2, ClothSpring::Flexion);
			}
			if (j > 0) {
				f = f - det_f(i, j, i, j - 1, ClothSpring::Structural);

				if (i > 0)
					f = f - det_f(i, j, i - 1, j - 1, ClothSpring::Shear);
				if (j > 1)
					f = f - det_f(i, j, i, j - 2, ClothSpring::Flexion);
			}
			m_Particles[j + i * m_ResolutionX]->force = -f;
		}
	}
}

void Cloth::CalculateExternalForces(double delta_t) {
	ClothParticle *particle;
	static Vector3D g(0, -9.81, 0), F_g, F_r, F_w;

	for (int i = 0; i < m_ResolutionX * m_ResolutionY; i++) {
		particle = m_Particles[i];
		F_g = g * particle->mass; // gravity
		F_r = particle->velocity * -particle->damping;  // friction
		particle->force += F_r;
		if (particle->fixed == false)
			particle->force += F_g;
		Vector3D n = particle->normal;
		//F_w = n * (n * U_FLUID) * m_Fluid;   // TODO!
		particle->force += F_w;
		particle->acceleration = particle->force * (1 / particle->mass);
		m_Particles[i] = particle;
	}
}

inline Vector3D Cloth::Pos(int x, int y) {
	return m_Particles[x + m_ResolutionX * y]->position;
}

Vector3D Cloth::getNormal(int x, int y) {
	static Vector3D u, v, n;
	if (y == m_ResolutionY - 1)
		u = Pos(x, y) - Pos(x, y - 1);
	else
		u = Pos(x, y + 1) - Pos(x, y);
	if (x == m_ResolutionX - 1)
		v = Pos(x, y) - Pos(x - 1, y);
	else
		v = Pos(x + 1, y) - Pos(x, y);
	n = u % v;
	return n.Normalized();
}

ClothSpring* Cloth::findSpring(ClothParticle* from, ClothParticle* to,
		ClothSpring::SpringTypes type) {
	int i = 0;
	if (!from || !to)
		exit(0);

	while (m_Springs[i]) {
		if (((m_Springs[i]->particle_A == from)
				&& (m_Springs[i]->particle_B == to))
				|| ((m_Springs[i]->particle_A == to)
						&& (m_Springs[i]->particle_B == from)))
			return m_Springs[i];
		i++;
	}
	m_Springs[i] = new ClothSpring();
	m_Springs[i]->type = type;
	m_Springs[i]->particle_A = from;
	m_Springs[i]->particle_B = to;
	return m_Springs[i];
}

void Cloth::InitializeFaces() {
	int i, x, y;

	for (y = 0; y < m_ResolutionY - 1; y++) {
		for (x = 0; x < m_ResolutionX - 1; x++) {
			i = x + y * (m_ResolutionX - 1);
			faces[i].nw = i + y; // = x+(y*m_ResolutionX)
			faces[i].no = i + y + 1; // = x+1+(y*m_ResolutionX)
			faces[i].sw = i + m_ResolutionX + y; // = x+(y+1)*m_ResolutionX
			faces[i].so = i + m_ResolutionX + y + 1; // = x+1+(y+1)*m_ResolutionX
		}
	}
}

void Cloth::setCollision(Collision *collisionProxy) {
	this->collisionProxy = collisionProxy;
}

void Cloth::testCollision(Collision *cp) {
	if (!cp)
		return;
	cp->visit(this);
}

void Cloth::IncFluid(double dfluid) {
	m_Fluid += dfluid;
	if (m_Fluid > MAXFLUID)
		m_Fluid = MAXFLUID;
}

void Cloth::DecFluid(double dfluid) {
	m_Fluid -= dfluid;
	if (m_Fluid < MINFLUID)
		m_Fluid = MINFLUID;
}

void Cloth::IncFriction(double dfriction) {
	m_Friction += dfriction;
	if (m_Friction > MAXFRICTION)
		m_Friction = MAXFRICTION;
}

void Cloth::DecFriction(double dfriction) {
	m_Friction -= dfriction;
	if (m_Friction < MINFRICTION)
		m_Friction = MINFRICTION;
}

void Cloth::IncMass(double dmass) {
	int i = 0;
	for (i = 0; i < m_ResolutionX * m_ResolutionY; i++) {
		m_Particles[i]->mass += dmass;
		if (m_Particles[i]->mass > MASSMAX)
			m_Particles[i]->mass = MASSMAX;
		else if (m_Particles[i]->mass < MASSMIN)
			m_Particles[i]->mass = MASSMIN;
	}
}

void Cloth::IncSuspension(double dstiffness) {
	int i = 0;
	while (m_Springs[i]) {
		m_Springs[i]->stiffness += dstiffness;
		i++;
	}
}

void Cloth::IncDamping(double ddamping) {
	int i = 0;
	for (i = 0; i < m_ResolutionX * m_ResolutionY; i++) {
		m_Particles[i]->damping += ddamping;
		if (m_Particles[i]->damping > DAMPINGMAX)
			m_Particles[i]->damping = DAMPINGMAX;
		else if (m_Particles[i]->damping < DAMPINGMIN)
			m_Particles[i]->damping = DAMPINGMIN;
	}
}

#endif
