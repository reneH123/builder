#ifndef CYLINDER_CPP_
#define CYLINDER_CPP_

#include <GL/glut.h>
#include "cloth.hpp"

Cylinder::Cylinder() {
	qobj = gluNewQuadric();
}

void Cylinder::Paint() {
	glColor3f(0.6, 0.8, 0.1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glRotatef(-90, 1, 0, 0);
	gluCylinder(qobj, BASERADIUS, TOPRADIUS, HEIGHT, SLICES, STACKS);
	glPopMatrix();
}

void Cylinder::visit(Cloth *cloth) {
	ClothParticle *p;
	Vector3D v;
	int x, y;

	for (y = 0; y < cloth->m_ResolutionY; y++)
		for (x = 0; x < cloth->m_ResolutionX; x++) {
			p = cloth->m_Particles[x + y * cloth->m_ResolutionX];
			p->fixed = false;
			v = p->position;
			v.y = 0;
			if ((p->position.y <= HEIGHT)
					&& (v.sqrlength()
							<= (TOPRADIUS * TOPRADIUS + 0.5))) {
				p->force += p->position;
				p->velocity += p->position;
				p->force += p->velocity * cloth->m_Friction;
			}
		}
}

#endif
