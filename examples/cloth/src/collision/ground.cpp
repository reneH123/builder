#ifndef GROUND_CPP_
#define GROUND_CPP_

#include "geometry.hpp"
#include "include.hpp"
#include <string.h>

Ground::Ground(){
	reSize(50, 50, 50);
	memcpy(&vertices, cubeVertices, sizeof(vertices));
	for (int i = 0; i < maxVertices; i++) {
		vertices[i] = vertices[i].ElementwiseMultiply(size);
	}
}

void Ground::Paint() {
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glTranslated(0, -10, 0);
	for (int i = 0; i < NUMCUBEEDGES; i++) {
		glColor3f(0.7, 0.9, 0.2);
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3dv(vertices[cubeEdges[i][0]].array);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3dv(vertices[cubeEdges[i][1]].array);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3dv(vertices[cubeEdges[i][2]].array);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3dv(vertices[cubeEdges[i][3]].array);
		glEnd();
	}
	glPopMatrix();
}

void Ground::visit(Cloth *cloth) {
	ClothParticle * p;
	static int x, y;

	for (y = 0; y < cloth->m_ResolutionY; y++) {
		for (x = 0; x < cloth->m_ResolutionX; x++) {
			p = cloth->m_Particles[x + y * cloth->m_ResolutionX];
			if (p->position.y < -10) {
				p->fixed = true;
			}
		}
	}
}

#endif
