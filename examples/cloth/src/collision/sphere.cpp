#ifndef SPHERE_CPP_
#define SPHERE_CPP_

#include <GL/glut.h>
#include "cloth.hpp"

Sphere::Sphere(){
	qobj = gluNewQuadric();
}

void Sphere::Paint() {
	glColor3d(0.5, 0.5, 0.7);
	gluSphere(qobj, RADIUS, SLICES, STACKS);
}

void Sphere::visit(Cloth *cloth) {
	static ClothParticle *p;
	static int i;
	static int maxParticles = cloth->m_ResolutionX * cloth->m_ResolutionY;
	for (i = 0; i < maxParticles; i++) {
		p = cloth->m_Particles[i];
		//p->fixed = false; // TODO!
		if (p->position.sqrlength() <= (RRADIUSONE+1.)) {
			//p->force += p->position;  // TODO!
			p->velocity+= p->position.Normalized();
			// friction
			p->force += p->velocity * cloth->m_Friction;
		}
	}
}

#endif
