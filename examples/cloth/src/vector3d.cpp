#ifndef VECTOR3D_CPP_
#define VECTOR3D_CPP_

#include "vector3d.hpp"
#include <math.h>

Vector3D::Vector3D() {
	this->x = 0;
	this->y = 0;
	this->z = 0;
}
Vector3D::Vector3D(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
Vector3D::Vector3D(const double values[3]) {
	Vector3D(values[0], values[1], values[2]);
}

const double& Vector3D::operator[](const int i) const {
	return this->array[i];
}

const Vector3D& Vector3D::operator=(const Vector3D& v) {
	DuplicateFrom(v);
	return (*this);
}

const bool Vector3D::operator==(const Vector3D& v) {
	if ((v.x == this->x) && (v.y == this->y) && (v.z == this->z))
		return true;
	return false;
}

void Vector3D::DuplicateFrom(const Vector3D& source) {
	this->x = source.x;
	this->y = source.y;
	this->z = source.z;
}

Vector3D Vector3D::operator+(const Vector3D& v) const {
	Vector3D ret(this->x + v.x, this->y + v.y, this->z + v.z);
	return ret;
}

Vector3D Vector3D::operator+=(const Vector3D& v) {
	Vector3D ret(this->x + v.x, this->y + v.y, this->z + v.z);
	DuplicateFrom(ret);
	return ret;
}

Vector3D Vector3D::operator-() const {
	return Vector3D(-this->x, -this->y, -this->z);
}

Vector3D Vector3D::operator-(const Vector3D& v) const {
	Vector3D ret(this->x - v.x, this->y - v.y, this->z - v.z);
	return ret;
}

double Vector3D::operator*(const Vector3D& v) const {
	return v.x * this->x + v.y * this->y + v.z * this->z;
}

double Vector3D::sqrlength() const {
	return this->x * this->x + this->y * this->y + this->z * this->z;
}

double Vector3D::operator *(Vector3D& source) {
	return source.x * this->x + source.y * this->y + source.z * this->z;
}

Vector3D Vector3D::operator%(Vector3D& source) {
	Vector3D ret(this->y * source.z - this->z * source.y,
			this->z * source.x - this->x * source.z,
			this->x * source.y - this->y * source.x);
	return ret;
}

Vector3D Vector3D::operator*(const double& scalar) const {
	return Vector3D(this->x * scalar, this->y * scalar, this->z * scalar);
}

Vector3D Vector3D::ElementwiseMultiply(Vector3D& source) {
	return Vector3D(this->x * source.x, this->y * source.y, this->z * source.z);
}

Vector3D Vector3D::ElementwiseDivide(Vector3D& source) {
	return Vector3D(this->x / source.x, this->y / source.y, this->z / source.z);
}

#endif
