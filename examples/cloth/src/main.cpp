/*
 * Author: René Haberland, June 2006
 *  Licensed under Lesser GPL 3
 * Simulation of 'Provot's Cloth' (Xavier Provot and INRIA. Deformation Constraints in a Mass-Spring Model to Describe Rigid Cloth Behavior
Graphics Interface, p.147-154, 1996)
 *
 *   - uses modified software from Benjamin Kalytta (2012, Bitmap Loader)
 *   - is not error-free and contains many TODOs, use only if your are brave and on your own risk
 * */

#include "cloth.hpp"
#include "geometry.hpp"
#include "include.hpp"
#include <GL/glut.h>
#include <stdio.h>

Cloth *myCloth;
Collision *proxy;

int rotateUpDown, rotateRightLeft, distance;
double aspect_ratio;

// simulation parameters
bool fluid_Activated = 0;
bool stiffness_Activated = 0;
bool damping_Activated = 0;
bool friction_Activated = 0;
//bool box_Activated=1;  // TODO
//bool cylinder_Activated=0;
//bool sphere_Activated=0;


void MenuFunction(int selected) {
	switch (selected) {
	case FLUID:
		inv(fluid_Activated);
		break;
	case FRICTION:
		inv(friction_Activated);
		break;
	case STIFFNESS:
		inv(stiffness_Activated);
		break;
	case DAMPING:
		inv(damping_Activated);
		break;
	case IEULER:
		myCloth->ChooseIntegrator(0);
		break;
	case RUNGEKUTTA:
		myCloth->ChooseIntegrator(1);
		break;
	case MIDPOINT:
		myCloth->ChooseIntegrator(2);
		break;
	case BOX: {
		delete proxy;
		proxy = new Box();
		myCloth->setCollision(proxy);
		myCloth->ReInitialize();
		break;
	}
	case CYLINDER: {
		delete proxy;
		proxy = new Cylinder();
		myCloth->setCollision(proxy);
		myCloth->ReInitialize();
		break;
	}
	case SPHERE: {
		delete proxy;
		proxy = new Sphere();
		myCloth->setCollision(proxy);
		myCloth->ReInitialize();
		break;
	}
	case NEW:
		myCloth->ReInitialize();
		break;
	case NORMALS:
		myCloth->InvertNormalsActivated();
		break;
	case EXIT:
		exit(0);
		break;
	case FLUIDINC:
		myCloth->IncFluid(0.014);
		break;
	case FLUIDDEC:
		myCloth->DecFluid(0.014);
		break;
	case FRICTIONINC:
		myCloth->m_Friction += 10;
		break;
	case FRICTIONDEC:
		myCloth->m_Friction -= 10;
		break;
	case STIFFINC:
		myCloth->IncSuspension(+1.);
		break;
	case STIFFDEC:
		myCloth->IncSuspension(-1.);
		break;
	case DAMPINGINC:
		myCloth->IncDamping(+0.1);
		break;
	case DAMPINGDEC:
		myCloth->IncDamping(-0.1);
		break;
	}
}

void Initialize() {
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //.. TODO
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glEnable(GL_DEPTH_TEST);
	gluPerspective(40,320.0/200.0,0.01,100);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_FALSE); // ..

	myCloth = new Cloth();
	proxy = new Sphere();
	myCloth->setCollision(proxy);

	glEnable(GL_NORMALIZE);

	// set up menu
	int phys_id=glutCreateMenu(MenuFunction);
	 glutAddMenuEntry("viscosity", FLUID);
	 glutAddMenuEntry("friction", FLUID);
	 glutAddMenuEntry("stiffness", STIFFNESS);
	 glutAddMenuEntry("damping", DAMPING);

	int physch_id=glutCreateMenu(MenuFunction);
	 glutAddMenuEntry("Increase viscosity", FLUIDINC);
	 glutAddMenuEntry("Decrease viscosity", FLUIDDEC);
	 glutAddMenuEntry("Increase friction", FRICTIONINC);
	 glutAddMenuEntry("Decrease friction", FRICTIONDEC);
	 glutAddMenuEntry("Increase stiffness", STIFFINC);
	 glutAddMenuEntry("Decrease stiffness", STIFFDEC);
	 glutAddMenuEntry("Increase damping", DAMPINGINC);
	 glutAddMenuEntry("Decrease damping", DAMPINGDEC);

	 int bew_id=glutCreateMenu(MenuFunction);
	 glutAddMenuEntry("Implicit Euler", IEULER);
	 glutAddMenuEntry("Midpoint method", MIDPOINT);
	 glutAddMenuEntry("Runge-Kutta method", RUNGEKUTTA);

	 int koll_id=glutCreateMenu(MenuFunction);
	 glutAddMenuEntry("Box", BOX);
	 glutAddMenuEntry("Cylinder", CYLINDER);
	 glutAddMenuEntry("Sphere", SPHERE);

	 int menu_id=glutCreateMenu(MenuFunction);
	 glutAddMenuEntry("Refresh",NEW);
	 glutAddMenuEntry("Draw normals", NORMALS);
	 glutAddSubMenu("Switch physical measure (on/off)",phys_id);
	 glutAddSubMenu("Change physical measure",physch_id);
	 glutAddSubMenu("Kinetics solver",bew_id);
	 glutAddSubMenu("Collision object",koll_id);
	 glutAddMenuEntry("Quit simulation", EXIT);
	 glutAttachMenu(GLUT_RIGHT_BUTTON);
}

Box *box=new Box();  // TODO: to be removed

void Paint() {
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40, 1., 1, 100);

	glTranslatef(0, 0, -distance);
	glRotated(rotateUpDown, 1, 0, 0);
	glRotated(rotateRightLeft, 0, 1, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	myCloth->Paint();
	//box->Paint(); // TODO: to be removed
	glutSwapBuffers();
}

void Idle() {
	myCloth->Step(0.01);  // 0.01 is stable
	Paint();
}

void SpecialKeyboardFunction(int key, int x, int y) {
	if (key == GLUT_KEY_UP)
		if (rotateUpDown < 90)
			rotateUpDown += 5;
	if (key == GLUT_KEY_DOWN)
		if (rotateUpDown > -90)
			rotateUpDown -= 5;
	if (key == GLUT_KEY_LEFT)
		rotateRightLeft -= 5;
	if (key == GLUT_KEY_RIGHT)
		rotateRightLeft += 5;
	if (key == GLUT_KEY_PAGE_UP)
		distance -= 1;
	if (key == GLUT_KEY_PAGE_DOWN)
		distance += 1;
	if (key==GLUT_KEY_HOME) myCloth->ReInitialize();
}

void KeyboardFunction(unsigned char key, int x, int y) {
	// you can check key for all keys represented by an ASCII-code
	if (key == KEY_PLUS) // "+"-key
	{
		if (fluid_Activated)
			myCloth->IncFluid(0.7972);
		if (friction_Activated)
			myCloth->m_Friction += 10;
		if (stiffness_Activated)
			myCloth->IncSuspension(+0.5);
		if (damping_Activated)
			myCloth->IncDamping(0.005);
	}
	if (key == KEY_MINUS) {
		if (fluid_Activated)
			myCloth->IncFluid(-0.7972);
		if (friction_Activated)
			myCloth->m_Friction -= 10;
		if (stiffness_Activated)
			myCloth->IncSuspension(-0.5);
		if (damping_Activated)
			myCloth->IncDamping(-0.005);
	}
	if (key == 27)
		exit(0);
}

void ReshapeFunc(int width, int height) {
	glViewport(0, 0, width, height);
	aspect_ratio = 1.0f;
}

int main(int argc, char* argv[]) {
	rotateUpDown = 10;
	rotateRightLeft = 160;
	distance = 40;
	aspect_ratio = 1.0f;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutCreateWindow("Provot's Cloth Simulation  by Rene Haberland 2006-2016");
	Initialize();
	glutDisplayFunc(Paint);
	glutIdleFunc(Idle);
	glutDisplayFunc(Paint);
	glutSpecialFunc(SpecialKeyboardFunction);
	glutReshapeFunc(ReshapeFunc);
	glutKeyboardFunc(KeyboardFunction);
	glutMainLoop();
	return 0;
}
