#ifndef INCLUDE_HPP_
#define INCLUDE_HPP_

#include "vector3d.hpp"

#define FLUID		1
#define STIFFNESS	2
#define DAMPING   	5
#define IEULER		6
#define MIDPOINT	7
#define RUNGEKUTTA	8
#define BOX		    9
#define CYLINDER	10
#define SPHERE		11
#define NORMALS	    12
#define FRICTION	13
#define EXIT		14

#define STIFFINC	    16
#define STIFFDEC	    17
#define SHEAREINC		18
#define SHEARDEC		19
#define DAMPINGINC	    22
#define DAMPINGDEC	    23
#define FLUIDINC		24
#define FLUIDDEC		25
#define FRICTIONINC		26
#define FRICTIONDEC		27

#define NEW  			30

#define KEY_PLUS  43
#define KEY_MINUS 45

#define inv(x)\
(x?x=0:x=1)

#define MASSMAX 3.0
#define MASSMIN 0.2

#define DAMPINGMAX 0.5
#define DAMPINGMIN 0.01
#define MAXFLUID 3.2029
#define MINFLUID 0.811
#define MAXFRICTION 100
#define MINFRICTION 0

#define SIZEGROUND_X 50
#define SIZEGROUND_Y 50
#define SIZEGROUND_Z 50

static const Vector3D u_fluid(0.1,0,0);

// cube mesh
#define NUMCUBEEDGES  6
#define NUMCUBEVERTICES 8

static const int cubeEdges[6][4] = { { 0, 1, 2, 3 }, { 2, 1, 5, 6 }, { 7, 6, 5,
		4 }, { 4, 0, 3, 7 }, { 2, 6, 7, 3 }, { 4, 5, 1, 0 } };

static const Vector3D cubeVertices[8] = { Vector3D(-1, -1, -1), Vector3D(+1,
		-1, -1), Vector3D(+1, +1, -1), Vector3D(-1, +1, -1), Vector3D(-1, -1,
		+1), Vector3D(+1, -1, +1), Vector3D(+1, +1, +1), Vector3D(-1, +1, +1) };

#endif
