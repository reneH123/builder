#ifndef VECTOR3D_HPP_
#define VECTOR3D_HPP_

#include <math.h>

class Vector3D {
public:
	Vector3D();
	Vector3D(double x, double y, double z);
	Vector3D(const double values[3]);

	union {
		struct {
			double x, y, z;
		};
		double array[3];
	};

	const bool operator==(const Vector3D& rhs);
	const Vector3D& operator=(const Vector3D& rhs);
	const double& operator[](const int i) const;

	const static Vector3D ZERO() {
		return Vector3D(0, 0, 0);
	}
	Vector3D operator+(const Vector3D& rhs) const;
	Vector3D operator+=(const Vector3D& rhs);

	Vector3D operator-(const Vector3D& rhs) const;
	Vector3D operator-() const;
	Vector3D operator*(const double& scalar) const;
	double operator *(Vector3D& source);
	double operator*(const Vector3D& v) const;
	Vector3D operator%(Vector3D& source);

	inline double length() const {
		return sqrt(x * x + y * y + z * z);
	}
	inline Vector3D Normalized() const {
		double length = this->length();

		Vector3D ret;
		if (length != 0) {
			ret = Vector3D(this->x / length, this->y / length, this->z / length);
			return ret;
		}
		return ret;
	}
	double sqrlength() const;
	Vector3D ElementwiseMultiply(Vector3D& source);
	Vector3D ElementwiseDivide(Vector3D& source);
private:
	void DuplicateFrom(const Vector3D& source);
};

#endif
