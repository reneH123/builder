#ifndef CLOTHPARTICLE_HPP
#define CLOTHPARTICLE_HPP

#include "vector3d.hpp"
#include "clothspring.hpp"

// classes for a mass-spring system

class ClothSpring;  // forward declaration for the class in the same file

class ClothParticle {
public:
	ClothParticle();
	Vector3D position, velocity, acceleration, force, normal;
	double mass;
	double damping;
	ClothSpring **links;
	bool fixed;
};

#endif
