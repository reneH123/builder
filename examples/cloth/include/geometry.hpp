#ifndef GEOMETRY_HPP_
#define GEOMETRY_HPP_

#include "cloth.hpp"
#include <GL/glut.h>
#include "vector3d.hpp"

class Cloth; // forward declaration


// interface for a collision proxy
class Collision{
protected:
	Vector3D size;
public:
	virtual ~Collision(){}
	virtual void visit(Cloth *cloth){}
	virtual void Paint(){}
    void reSize(int sizeX,int sizeY,int sizeZ)
	{
		this->size=Vector3D(sizeX,sizeY,sizeZ);
	}
};

class Sphere: public Collision {
private:
	static const GLdouble RADIUS = 5;
	static const GLdouble RRADIUSONE = 26;
	static const GLdouble SLICES = 100; // 200
	static const GLdouble STACKS = 50; // 80

	GLUquadricObj *qobj;
public:
	Sphere();
	//~Sphere();
	void Paint();
	void visit(Cloth *cloth);
};


class Ground: public Collision {
private:
	static const int maxVertices = 8;
	Vector3D vertices[maxVertices];
	GLuint texture[1];
public:
	Ground();
	void Paint();
	void visit(Cloth *cloth);
};

class Cylinder: public Collision {
private:
	static const GLdouble BASERADIUS = 3;
	static const GLdouble TOPRADIUS = 3;
	static const GLdouble HEIGHT = 5;
	static const GLdouble SLICES = 200;
	static const GLdouble STACKS = 80;

	GLUquadricObj *qobj;
public:
	Cylinder();
	void Paint();
	void visit(Cloth *cloth);
};

class Box: public Collision {
private:
	static const int maxVertices = 8;
	Vector3D vertices[maxVertices];
public:
	Box();
	void Paint();
	void visit(Cloth *cloth);
};

#endif
