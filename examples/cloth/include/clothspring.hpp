#ifndef CLOTHSPRING_HPP_
#define CLOTHSPRING_HPP_

#include "clothparticle.hpp"

class ClothParticle; // forward declaration

class ClothSpring{
private:
	void create(ClothParticle* A, ClothParticle* B);
public:
	ClothSpring();

	enum SpringTypes {
		Structural, Shear, Flexion
	};
	double initialLength;
	ClothParticle* particle_A;
	ClothParticle* particle_B;
	double stiffness;
	SpringTypes type;
};

#endif
